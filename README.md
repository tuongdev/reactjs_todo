# Task Management
Task Management is a "pet" project where I pratice my knowledge of ReactJs and NodeJS

## Table of contents
* [Features](#features)
* [Components](#components)
* [Technologies](#technologies)
* [Installation](#installation)

## Components
* Task Management Client
* Task Management API

## Features
Containing simple functions:
* User (CRUD, login)
* Task (CRUD)
* Sprint (CRUD)
* Schedule meeting (CRUD)

## Technologies
* React Js: HTML enhanced for web apps (front-end)
* Axios: Promise based HTTP client for the browser and node.js (front-end)
* Node Js]: evented I/O for the backend (back-end)
* Express Js: fast node.js network app framework (back-end)
* Sqlite3: local database (back-end)

## Installation
Task Management requires [Node.js](https://nodejs.org/) v12+ to run.

Install the dependencies

```sh
yarn install 
```
or
```sh
npm install
```
Build
```sh
yarn build
```

** Run and debug back-end API (task-managment-api) **

to Run
```sh
yarn start
```
to Debug
```
yarn debug

** Run and debug Client (task-management) **

```sh
yarn start
```

## References
[https://dev.to/ms_yogii/useaxios-a-simple-custom-hook-for-calling-apis-using-axios-2dkj](https://dev.to/ms_yogii/useaxios-a-simple-custom-hook-for-calling-apis-using-axios-2dkj)
[https://www.codementor.io/@olatundegaruba/nodejs-restful-apis-in-10-minutes-q0sgsfhbd](https://www.codementor.io/@olatundegaruba/nodejs-restful-apis-in-10-minutes-q0sgsfhbd)
[https://github.com/eldomagan/axios-rest](https://github.com/eldomagan/axios-rest)
[https://www.npmjs.com/package/react-error-boundary](https://www.npmjs.com/package/react-error-boundary)
[https://www.digitalocean.com/community/tutorials/how-to-add-login-authentication-to-react-applications](https://www.digitalocean.com/community/tutorials/how-to-add-login-authentication-to-react-applications)
