module.exports = (sequelize, Sequelize) => {
    const Task = sequelize.define("tasks", {
        id: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        taskname: {
            type: Sequelize.STRING,
            description: {
                type: Sequelize.STRING
            }
        },
        status: {
            type: Sequelize.INTEGER
        }
    })
    return Task;
}
