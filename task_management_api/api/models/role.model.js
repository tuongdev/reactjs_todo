module.exports = (sequelize, Sequelize) => {
    const Role = sequelize.define("roles", {
        id: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        rolename: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        }
    });
    return Role;
}