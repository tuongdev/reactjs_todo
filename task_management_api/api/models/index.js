const Sequelize = require("sequelize");
const { DB_CONFIG } = require("../config/config");

const sequelize = new Sequelize(
    DB_CONFIG.DB_NAME,
    DB_CONFIG.DB_USER,
    DB_CONFIG.DB_PWD,
    {
      host: DB_CONFIG.HOST,
      dialect: DB_CONFIG.DB_DIALECT,
      pool: {
        max: DB_CONFIG.DB_POOL_MAX,
        min: DB_CONFIG.DB_POOL_MIN,
        acquire: DB_CONFIG.DB_POOL_ACQUIRE,
        idle: DB_CONFIG.DB_POOL_IDLE
      },
      storage: DB_CONFIG.DB_SOURCE
    }
);

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.user = require("./user.model")(sequelize, Sequelize);
db.role = require("./role.model.js")(sequelize, Sequelize);
db.task = require("./task.model.js")(sequelize, Sequelize);
db.role.belongsToMany(db.user, {
    through: "user_roles",
    foreignKey: "roleId",
    otherKey: "userId"
});
db.user.belongsToMany(db.role, {
    through: "user_roles",
    foreignKey: "userId",
    otherKey: "roleId"
});
db.user.hasOne(db.task);
db.task.belongsTo(db.user, {
  foreignKey: {
    name: 'userId'
  }
})

module.exports = db;