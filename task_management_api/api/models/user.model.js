
module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("users", {
        id: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        username: {
            type: Sequelize.STRING, 
            allowNull: false, 
            unique: true
        },
        email: {
            type: Sequelize.STRING, 
            allowNull: false, 
            unique: true
        },
        password: {
            type: Sequelize.STRING, 
            allowNull: false, 
            unique: true
        },
    });
    return User;
}
