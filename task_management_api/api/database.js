const { v4: uuidv4 } = require('uuid');
// database
const db = require("./models/index");
const Role = db.role;
const initial = () => {
  Role.create({
    id: uuidv4(),
    rolename: "employee"
  });

  Role.create({
    id: uuidv4(),
    rolename: "admin"
  });
}

databaseInit = async() => {
  //force: true will drop the table if it already exists
  await db.sequelize.sync({force: false}).then(() => {
    console.log('Drop and Resync Database with { force: true }');
    initial();
  }).catch(err => console.log('There was an error: ', err));
  process.exit(0);
}

module.exports = databaseInit;  