const jwt = require("jsonwebtoken");
const config = require("../config/auth.config");
const db = require("../models");
const Users = db.user;

verifyToken = (req, res, next) => {
    let token = req.headers["x-access-token"];
    if (!token) {
        return res.status(403).send({
            message: "No token provied!"
        });
    }

    jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
            return res.status(401).send({
                message: "Unthorized!"
            });
        }
        req.userId = decoded.Id;
        next();
    });
}

isAdmin = (req, res, next) => {
    Users.findByPk(req.userId).then(user => {
        Users.getRoles().then(roles => {
            if(roles.find(f => f["name"] === "admin")) {
                next();
                return;
            }
        });
    });
}

isEmployee = (req, res, next) => {
    Users.findByPk(req.userId).then(user => {
        Users.getRoles().then(roles => {
            if(roles.find(f => f["name"] === "employee")) {
                next();
                return;
            }
        });
    });
}

const authJwt = {
    verifyToken: verifyToken,
    isAdmin: isAdmin,
    isEmployee: isEmployee
};

module.exports = authJwt;