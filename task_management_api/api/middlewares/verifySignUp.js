const db = require("../models");
const Roles = db.role;
const Users = db.user;

checkDuplicateUser = (req, res, next) => {
    Users.findOne({
        where: {
            username: req.body.username
        }
    }).then(user => {
        if(user) {
            res.status(400).send({
                message: "Failed! Username is already in use!"
            });
            return;
        }
    })
    Users.findOne({
        where: {
            email: req.body.email
        }
    }).then(user => {
        if(user) {
            req.status(400).send({
                message: "Failed! Email is already in use!"
            });
            return;
        }
        next();
    });
}

checkRolesExisted = (req, res, next) => {
    if(req.body.roles) {
        for (let i = 0; i < req.body.roles.length; i++) {
            if(!Roles.includes(req.body.roles[i])) {
                req.status(400).send({
                    message: "Failed! Role does not exist =" + req.body.roles[i]
                });
                return;
            }
        }
    }
    next();
}

const verifySignUp = {
    checkDuplicateUser: checkDuplicateUser,
    checkRolesExisted: checkRolesExisted
}

module.exports = verifySignUp;