const DB_CONFIG = {
    DB_SOURCE: "./api/db/taskDB.sqlite",
    DB_NAME: "taskDB",
    DB_DIALECT: "sqlite",
    DB_USER: "sa",
    DB_PWD: "adm123456!",
    DB_HOST: "0.0.0.0",
    DB_POOL_MAX: 5,
    DB_POOL_MIN: 0,
    DB_POOL_IDLE: 10000,
    DB_POOL_ACQUIRE:30000
}

const SERVER_CONFIG = {
    API_PORT: 3003,
    API_SECRET: "7305f75d2e2eda0f9a2a26c580ac4fe89e8239d11df46301d44f76cc88f9a300"
}
module.exports =  { DB_CONFIG, SERVER_CONFIG };