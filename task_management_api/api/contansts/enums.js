export const TaskStatus = {
    NEW: 0,
    IN_PROGESS: 1,
    DONE: 2
}

export const UserRole = {
    ADMIN: 0,
    EMPLOYEE: 1
}