const express = require('express');
const router  = express.Router(); 
var tasks = require('../controllers/task.controller');

router.get('/api/tasks', tasks.list_all_tasks)
router.post('/api/tasks', tasks.create_a_task);
router.get('/api/tasks/:id', tasks.read_a_task)
router.put('/api/tasks/:id', tasks.update_a_task)
router.delete('/api/tasks/:id',tasks.delete_a_task);

module.exports = router;