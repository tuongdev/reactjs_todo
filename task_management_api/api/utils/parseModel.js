

parseModel = (modelName, obj) => {
    var model = new window[modelName]();
    return Object.assign(model, obj);
}

module.exports = parseModel;