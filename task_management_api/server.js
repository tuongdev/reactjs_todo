const express = require('express');
const cors = require('cors');
const databaseInit = require("./api/database");
const { SERVER_CONFIG } = require("./api/config/config");
const listEndpoints = require("express-list-endpoints");
const cookieSession = require("cookie-session");

app = express();
port = SERVER_CONFIG.API_PORT;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(
  cookieSession({
    name: "bezkoder-session",
    secret: "COOKIE_SECRET", // should use as secret environment variable
    httpOnly: true,
    sameSite: 'strict'
  })
);

// var corsOptions = {
//   origin: "http://localhost:3000"
// };
app.use(cors());

databaseInit();

// routes
require("./api/routes/auth.routes")(app);
require("./api/routes/user.routes")(app);

app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});
app.listen(port);

console.log('Task Management RESTful API server started on: ' + port);

console.log(listEndpoints(app));