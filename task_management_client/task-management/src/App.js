import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { ErrorBoundary } from "react-error-boundary";
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Dashboard from "./components/App/Dashboard";
import Users from "./components/User/Users";
import Login from "./components/App/Login";
import Register from './components/App/Register';

function ErrorFallback({error, resetErrorBoundary}) {
  return (
    <div role="alert">
      <p>Something went wrong:</p>
      <pre>{error.message}</pre>
      <button onClick={resetErrorBoundary}>Try again</button>
    </div>
  )
}

function App() {
  return (
    <div className="App">
      <div className="wrapper">
        <div className="content">
          <ErrorBoundary
            FallbackComponent={ErrorFallback}
            onReset={() => {
              // reset the state of your app so the error doesn't happen again
            }}>     
            <Router>
                <Routes>
                  <Route path="/" element={<Dashboard />} />
                  <Route path="/users" element={<Users />} />
                  <Route path="/signup" element={<Register />} />
                  <Route path="/signin" element={<Login />} />
                </Routes>
            </Router>
          </ErrorBoundary>
        </div>
      </div>
    </div>
  );
}

export default App;
