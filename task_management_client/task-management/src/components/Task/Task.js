import React, {useState} from "react";
import EditTaskModal from "../Task/modals/EditTaskModal";

const Task = ({taskObj, deleteTask, updateTaskCmd, users}) => {
    const [modal, setModal] = useState(false);

    const handleDelete = () => {
        deleteTask(taskObj.id);
    }

    const toggle = () => {
        setModal(!modal);
    }

    const updateTask = async (obj) => {
      const isClose = await updateTaskCmd(obj);
      if (isClose)
        setModal(false);
    }

    const closeEdit = () => {
        setModal(false);
    }

    return (
        <div className="task-wrapper">
            <div className="task-top"></div>
            <div className="task-holder">
                <span className="task-header">
                    {taskObj.name}
                </span>
                <p className="task-desc">{taskObj.description}</p>
                <div className="task-bottom">
                    <div className="task-assign">
                        <i className="far fa-user-circle task-user"></i>
                        {taskObj.userName}
                    </div>
                    <div className="task-action">
                        <i className="far fa-edit act-edit"  onClick={() => setModal(true)}></i>
                        <i className="fas fa-trash-alt act-del"  onClick={handleDelete}></i>
                    </div>
                </div>
            </div>
            <EditTaskModal modal={modal} toggle={toggle} updateTask={updateTask} taskObj={taskObj} users={users} closeModal={closeEdit}/>
        </div>
    );
}

export default Task;