import React, { useState, useEffect, useMemo } from "react";
import { Button, Modal, ModalBody, ModalHeader, ModalFooter } from 'reactstrap';
import Select from "react-select";
import TaskModel from "../../../models/task.model";
import { TaskStatus } from "../../../contansts/enums";

const EditTaskModal = ({modal, toggle, users, updateTask, taskObj, closeModal }) => {
    const [taskName, setTaskName] = useState('');
    const [description, setDescription] = useState('');
    const [user, setUser] = useState({});
    const [status, setStatus] = useState({});

    const statusOptions = useMemo(() => [
        { label: 'NEW', value: TaskStatus.NEW },
        { label: 'IN PROGESS', value: TaskStatus.IN_PROGESS },
        { label: 'DONE', value: TaskStatus.DONE },
    ], []);
    const handleNameChange = (e) => {
        setTaskName(e.target.value);
    }

    const handleDescriptionChange = (e) => {
        setDescription(e.target.value);
    }

    const handleStatusChange = (e) => {
        setStatus({name: e.label, value: e.value});
    }

    const handleUserChange = (e) => {
        setUser({userName: e.label, userId: e.value});
    }

    useEffect(()=>{
        const getUser = () => {
            const userIndex = users.findIndex(f => f.id === taskObj.userId);
            if(userIndex !== -1) {
                setUser({userName: users[userIndex].name, userId: users[userIndex].id});
            }
        }
        getUser()
    }, [taskObj.userId, users])

    useEffect(()=>{
        const getStatus = () => {
            const statusIndex = statusOptions.findIndex(f => f.value === taskObj.status);
            if(statusIndex !== -1) {
                setStatus({name: statusOptions[statusIndex].label, value: statusOptions[statusIndex].value});
            }
        }
        getStatus()
    }, [statusOptions, taskObj.status])

    useEffect(() => {
        setTaskName(taskObj.name);
        setDescription(taskObj.description);
    }, [taskObj])

    const handleUpdate = (e) => {
        const taskModel = new TaskModel({
            id: taskObj.id,
            name: taskName,
            description: description,
            status: status.value,
            userId: user.userId
        });
        updateTask(taskModel);
    }

    const handleClose = () => {
        closeModal();
    }

    return (
        <Modal isOpen={modal} toggle={toggle}>
            <ModalHeader toggle={toggle}>
                Update task
            </ModalHeader>

            <ModalBody>
               <form>
                   <div className="form-group">
                       <label>Task Name</label>
                       <input type="text" className="form-control" value={taskName} onChange={handleNameChange}/>
                   </div>
                   <div className="form-group">
                       <label>Assgin To</label>
                       <Select value={{label: user.userName, value: user.userId}}
                            options={users.map(a => ({label: a.name, value: a.id}))}
                            onChange={handleUserChange} />
                   </div>                     
                   <div className="form-group">
                        <label>Description</label>
                       <textarea rows="5" className="form-control" value={description} onChange={handleDescriptionChange}></textarea>
                   </div>
                   <div className="form-group">
                        <label>Status</label>
                        <Select value={{label: status.name, value: status.value}}
                            options={statusOptions}  onChange={handleStatusChange} />
                   </div>                     
               </form>
            </ModalBody>

            <ModalFooter>
                <Button color="primary" onClick={handleUpdate}> Update </Button> {' '}
                <Button onClick={handleClose}> Cancel </Button>
            </ModalFooter>
        </Modal>
    );
};

export default EditTaskModal;