import React, { useState } from "react";
import { Button, Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import Select from "react-select";
import {v4 as uuidv4} from "uuid";
import { TaskStatus } from "../../../contansts/enums";
import TaskModel from "../../../models/task.model";

const CreateTaskModal = ({modal, toggle, save, users, closeModal}) => {
    const [taskName, setTaskName] = useState('');
    const [description, setDescription] = useState('');
    const [status, setStatus] = useState(0);
    const [user, setUser] = useState('');

    const statusOptions = [
        { label: 'NEW', value: TaskStatus.NEW },
        { label: 'IN PROGESS', value: TaskStatus.IN_PROGESS },
        { label: 'DONE', value: TaskStatus.DONE },
    ];

    const handleNameChange = (e) => {
        setTaskName(e.target.value);
    }

    const handleDescriptionChange = (e) => {
        setDescription(e.target.value);
    }

    const handleUserChange = (e) => {
        setUser({userName: e.label, userId: e.value});
    }

    const handleSave = () => {
        const taskModel = new TaskModel({
            id: uuidv4(),
            name: taskName,
            description: description,
            status: status,
            userId: user.userId
        });
        save(taskModel);
    }

    const handleClose = () => {
        closeModal();
    }

    const handleStatusChange = (e) => {
        setStatus(e.value);
    }

    return (
        <Modal isOpen={modal} toggle={toggle}>
            <ModalHeader toggle={toggle}>
                Create task
            </ModalHeader>

            <ModalBody>
               <form>
                   <div className="form-group">
                       <label>Task Name</label>
                       <input type="text" className="form-control" value={taskName} onChange={handleNameChange}/>
                   </div>
                   <div className="form-group">
                       <label>Assgin To</label>
                       <Select options={users.map(a => ({label: a.name, value: a.id}))} onChange={handleUserChange} />
                   </div>                   
                   <div className="form-group">
                        <label>Description</label>
                       <textarea rows="5" className="form-control" value={description} onChange={handleDescriptionChange}></textarea>
                   </div>
                   <div className="form-group">
                        <label>Status</label>
                        <Select options={statusOptions}  onChange={handleStatusChange} />
                   </div>                   
               </form>
            </ModalBody>

            <ModalFooter>
                <Button color="primary" onClick={handleSave}> Save </Button>
                <Button onClick={handleClose}> Cancel </Button>
            </ModalFooter>
        </Modal>
    );
};

export default CreateTaskModal;