import React, { useState } from "react";
import { Button, Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import { UserRole } from "../../../contansts/enums";
import UserModel from "../../../models/user.model";
import md5 from "md5";

const CreateUserModal = ({createModal, toggle, save, closeModal}) => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [role, setRole] = useState('')

    const handleNameChange = (e) => {
        setName(e.target.value);
    }
    
    const handleEmailChange = (e) => {
        setEmail(e.target.value);
    }

    const handlePasswordChange = (e) => {
        setPassword(e.target.value);
    }

    const handleOptionChange = (e) => {
        setRole(e.target.value);
    }

    const handleSave = () => {
        const userModel = new UserModel({
            name: name,
            password: md5(password),
            email: email,
            role: role   
        });
        save(userModel);
    }

    const handleClose = () => {
        closeModal();
    }

    return (
        <Modal isOpen={createModal} toggle={toggle}>
            <ModalHeader toggle={toggle}>
                Create User
            </ModalHeader>

            <ModalBody>
               <form>
                   <div className="form-group">
                       <label>User Name</label>
                       <input type="text" className="form-control" value={name} onChange={handleNameChange}/>
                   </div>                 
                   <div className="form-group">
                        <label>Email</label>
                        <input type="text" className="form-control" value={email} onChange={handleEmailChange} />
                   </div>
                   <div className="form-group">
                        <label>Password</label>
                        <input type="password" className="form-control" value={password} onChange={handlePasswordChange} />
                   </div>                   
                   <div className="form-group">
                        <label>Role</label>
                        <div className="radio">
                            <label style={{paddingRight: "10px", cursor:"pointer"}}>
                                <input type="radio" name={role} value={UserRole.ADMIN} style={{cursor:"pointer"}}
                                    onChange={handleOptionChange} />
                                <span style={{paddingLeft: "5px", lineHeight:"25px", verticalAlign: "bottom"}}>ADMIN</span>
                            </label>
                            <label style={{cursor:"pointer"}}>
                                <input type="radio" name={role} value={UserRole.EMPLOYEE} style={{cursor:"pointer"}}
                                    onChange={handleOptionChange} />
                                <span style={{paddingLeft: "5px", lineHeight:"25px", verticalAlign: "bottom"}}>EMPLOYEE</span>
                            </label>
                        </div>                        
                   </div>                   
               </form>
            </ModalBody>

            <ModalFooter>
                <Button color="primary" onClick={handleSave}> Save </Button>
                <Button onClick={handleClose}> Cancel </Button>
            </ModalFooter>
        </Modal>
    );    
}

export default CreateUserModal;