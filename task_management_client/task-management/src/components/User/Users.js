import React, {useEffect, useState} from "react";
import "./user.css";
import { useErrorHandler } from "react-error-boundary";
import { GetAllUser, AddUser, DeleteUser } from "../../services/user.service";
import CreateUserModal from "./modals/CreateUserModal";
import useToken from "../App/useToken";
import { UserRole } from "../../contansts/enums";

const Users = () => {
    const [createModal, setModal] = useState(false);
    const [userList, setUserList] = useState([]);
    const { loggedUser } = useToken({});
    const isAuthor = (loggedUser.role === UserRole.ADMIN) ? true : false;
    const toggle = () => setModal(!createModal);
    const handleError = useErrorHandler();

    useEffect(() => {
        (async function() {
            const usersResponse = await GetAllUser();
            if(usersResponse) {
                setUserList(usersResponse.data.data);
            }
        })();
    }, [])

    const saveUser = async(userObj) => {
        setModal(false);
        const createResponse = await AddUser(userObj);
        if (createResponse.data.message === 'success') {
            setUserList([...userList, userObj]);
        } else if (createResponse.data.statusCode === 400) {
            handleError(createResponse.data.message);
        }        
    }

    const handleDelete = async (userId) => {
        const deleteUserResponse = await DeleteUser(userId);
        if (deleteUserResponse.data.message === 'success') {
            const foundIndex = setUserList.findIndex(f => f.id === userId);
            userList.splice(foundIndex, 1);
            setUserList([...userList])
        } else if (deleteUserResponse.data.statusCode === 400) {
            handleError(deleteUserResponse.data.message);
        }
    }

    return (
        <>
            <div className="header text-center">
                <h3>User List</h3>
                <button className="btn btn-primary mt-2" disabled={!isAuthor} onClick={() => setModal(true)}>Create User</button>
            </div>
            <div className="user-container">
                {userList && userList.map((obj, index) => (
                    <div key={obj.id} style={{marginLeft: "10px"}}>
                        <div className="user-wrapper">
                            <div className="user-top"></div>
                            <div className="user-holder">
                                <div className="user-info">
                                   <span className="user-label"> <i className="far fa-user" ></i></span>
                                   <span className="user-value">{obj.name}</span>
                                </div>
                                <div className="user-info">
                                   <span className="user-label"> <i className="far fa-envelope" ></i></span>
                                   <span className="user-value">{obj.email}</span>
                                </div> 
                                <div className="user-info">
                                   <span className="user-label"> <i className="far fa-id-card" style={{color:'orangered'}}></i></span>
                                   <span className="user-value">{obj.role}</span>
                                </div>                                                                                              
                            </div>
                            <div className="user-bottom">
                                <div className="user-action">
                                    <i className="far fa-edit act-edit" ></i>
                                    <i className="fas fa-trash-alt act-del" disabled={isAuthor} onClick={handleDelete}></i>
                                </div>
                            </div>                            
                        </div>
                    </div>
                ))}
            </div>
            <CreateUserModal createModal={createModal} toggle={toggle} save={saveUser} closeModal={() => setModal(false)} />                 
        </>
    );
}

export default Users;