import React, { useState } from "react";
import { useErrorHandler } from "react-error-boundary";
import { SignIn } from "../../services/auth.service"
import md5 from "md5";

const Login = ({ setToken }) => {
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    const handleError = useErrorHandler();
    
    const handleLogin = async(e) => {
        e.preventDefault();
        const loginResponse = await SignIn(username, password);

        if (loginResponse.data.message === 'success') {
            setToken(loginResponse.data.data[0]);
        } else if (loginResponse.data.statusCode === 400) {
            handleError(loginResponse.data.message);
        }
    }

    return (
        <>
            <div className="text-center" style={{paddingTop: "250px"}}>
                <div className="logo">Login</div>
                <div className="login-form-1">
                    <form id="login-form" className="text-left" onSubmit={handleLogin}>
                        <div className="login-form-main-message"></div>
                        <div className="main-login-form">
                            <div className="login-group">
                                <div className="form-group">
                                    <label htmlFor="lg_username" className="sr-only">Username</label>
                                    <input type="text" className="form-control" id="lg_username" name="lg_username" 
                                        placeholder="username" onChange={e => setUserName(e.target.value)} />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="lg_password" className="sr-only">Password</label>
                                    <input type="password" className="form-control" id="lg_password" name="lg_password" 
                                        placeholder="password" onChange={e => setPassword(md5(e.target.value))}/>
                                </div>
                            </div>
                            <button type="submit" className="login-button"><i className="fa fa-chevron-right"></i></button>
                        </div>
                        <a href="/signup" className="link-primary"> Register</a>
                    </form>
                </div>
            </div>

        </>
    );
}

export default Login;