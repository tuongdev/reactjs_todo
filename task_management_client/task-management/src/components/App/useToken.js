import { useState } from 'react';

const useToken = () => {
  const getToken = () => {
    const tokenString = sessionStorage.getItem('loggedUser');
    const userToken = JSON.parse(tokenString);
    return userToken
  };
  const [loggedUser, setToken] = useState(getToken());

  const saveToken = userToken => {
    sessionStorage.setItem('loggedUser', JSON.stringify(userToken));
    setToken(userToken);
  };

  return {
    setToken: saveToken,
    loggedUser
  }
}

export default useToken;