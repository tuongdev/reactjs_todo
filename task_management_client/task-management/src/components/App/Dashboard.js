import React, {useEffect, useState} from "react";
import CreateTaskModal from "../Task/modals/CreateTaskModal";
import { useErrorHandler } from "react-error-boundary";
import Task from "../Task/Task";
import Login from "../App/Login";
import useToken from "../App/useToken";
import { GetAll, AddTask, UpdateTask, DeleteTask } from "../../services/task.service";
import { GetAllUser} from "../../services/user.service";
import { TaskStatus } from "../../contansts/enums";

const Dashboard = () => {
    const [modal, setModal] = useState(false);
    const [taskList, setTaskList] = useState([]);
    const [userList, setUserList] = useState([]);
    const { loggedUser, setToken } = useToken({});
    const toggle = () => setModal(!modal);
    const handleError = useErrorHandler();

    useEffect(() => {
        if(!loggedUser) {
            return <Login setToken={setToken} />;
        } else {
            (async function() {
                const taskResponse = await GetAll();
                if(taskResponse) {
                    setTaskList(taskResponse.data.data);
                }
                
                const userResponse = await GetAllUser();
                if (userResponse) {
                    setUserList(userResponse.data.data);
                }
            })();
        }
    }, [])

    const saveTask = async (taskObj) => {
        setModal(false);
        const createResponse = await AddTask(taskObj);
        if (createResponse.data.message === 'success') {
            setTaskList([...taskList, taskObj]);
        } else if (createResponse.data.statusCode === 400) {
            handleError(createResponse.data.message);
        }
    }

    const deleteTask = async (taskId) => {
        const deleteTaskResponse = await DeleteTask(taskId);
        if (deleteTaskResponse.data.message === 'success') {
            const foundIndex = taskList.findIndex(f => f.id === taskId);
            taskList.splice(foundIndex, 1);
            setTaskList([...taskList])
        } else if (deleteTaskResponse.data.statusCode === 400) {
            handleError(deleteTaskResponse.data.message);
        }
    }

    const updateTaskCmd = async (taskObj) => {
        const updateResponse = await UpdateTask(taskObj);
        if (updateResponse.data.message === 'success') {
            const editIndex = taskList.findIndex(f => f.id === taskObj.id);
            if(editIndex !== -1) {
                taskList[editIndex].id = taskObj.id;
                taskList[editIndex].name = taskObj.name;
                taskList[editIndex].description = taskObj.description;
                taskList[editIndex].status = taskObj.status;
                taskList[editIndex].userId = taskObj.userId;

                return true;
            }
            return false;
        } else if (updateResponse.data.statusCode === 400) {
            handleError(updateResponse.data.message);
        }
    }



    return (
        <>
            <div className="header text-center">
                <h3>Task List</h3>
                <button className="btn btn-primary mt-2" onClick={() => setModal(true)}>Create Task</button>
            </div>
            <div className="content">
                <div className="task-container">
                    <div className="task-item">
                        <h3>New</h3>
                        {taskList && taskList.filter(f => f.status === TaskStatus.NEW).map((obj, index) => (
                            <div key={obj.id}>
                                <Task taskObj={obj} deleteTask={deleteTask} updateTaskCmd={updateTaskCmd} users={userList} />
                            </div>
                        ))}
                    </div>
                    <div className="task-item">
                        <h3>In Process</h3>
                        {taskList && taskList.filter(f => f.status === TaskStatus.IN_PROGESS).map((obj, index) => (
                            <div key={obj.id}>
                                <Task taskObj={obj} deleteTask={deleteTask} updateTaskCmd={updateTaskCmd} users={userList} />
                            </div>
                        ))}
                    </div>
                    <div className="task-item">
                        <h3>Done</h3>
                        {taskList && taskList.filter(f => f.status === TaskStatus.DONE).map((obj, index) => (
                            <div key={obj.id}>
                                <Task taskObj={obj} deleteTask={deleteTask} updateTaskCmd={updateTaskCmd} users={userList} />
                            </div>
                        ))}                        
                    </div>
                </div>
            </div>
            <CreateTaskModal modal={modal} users={userList} toggle={toggle} save={saveTask} closeModal={() => setModal(false)} />
        </>
    )
};

export default Dashboard;
