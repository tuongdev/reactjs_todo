import React, { useState } from "react";
import  { Navigate } from 'react-router-dom'
import { useErrorHandler } from "react-error-boundary";
import { SignUp } from "../../services/auth.service"
import md5 from "md5";

const Register = () => {
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    const [email, setEmail] = useState();
    const [roles, setRoles] = useState();

    const handleError = useErrorHandler();
    
    const handleRegister = async(e) => {
        e.preventDefault();
        const registerUser = {
            username: username,
            password: password,
            email: email,
            roles: roles
        };
        const registerResponse = await SignUp(registerUser);
        if (registerResponse.data.message === 'success') {
            return <Navigate to='/signin' />
        } else if (registerResponse.data.statusCode === 400) {
            handleError(registerResponse.data.message);
        }
    }

    return (
        <>
            <div className="text-center" style={{paddingTop: "250px"}}>
                <div className="logo">Register</div>
                <div className="register-form-1">
                    <form id="register-form" className="text-left" onSubmit={handleRegister}>
                        <div className="register-form-main-message"></div>
                        <div className="main-register-form">
                            <div className="register-group">
                                <div className="form-group">
                                    <label htmlFor="rg_username" className="sr-only">Username</label>
                                    <input type="text" className="form-control" id="rg_username" name="rg_username" 
                                        placeholder="username" onChange={e => setUserName(e.target.value)} />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="rg_password" className="sr-only">Password</label>
                                    <input type="password" className="form-control" id="rg_password" name="rg_password" 
                                        placeholder="password" onChange={e => setPassword(md5(e.target.value))}/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="rg_email" className="sr-only">Email</label>
                                    <input type="email" className="form-control" id="rg_email" name="rg_email" 
                                        placeholder="email" onChange={e => setEmail(e.target.value)}/>
                                </div> 
                                <div className="form-group">
                                    <div class="form-check form-check-inline">
                                        <input className="form-check-input" type="radio" name="roles" id="is_employee" value="employee" />
                                        <label className="form-check-label" onChange={e => setRoles(e.target.value)} htmlFor="is_employee">Employee</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input className="form-check-input" type="radio" name="roles" id="is_admin" value="admin" />
                                        <label className="form-check-label" onChange={e => setRoles(e.target.value)} htmlFor="is_admin">Admin</label>
                                    </div>
                                </div>                                                                                            
                            </div>
                            <button type="submit" className="register-button"><i className="fa fa-chevron-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>

        </>
    );
}

export default Register;
