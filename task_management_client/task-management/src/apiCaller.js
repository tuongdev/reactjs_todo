
import { API_CONFIG } from "./contansts/config";
import axios from "axios";

const instance = axios.create({
  baseURL: API_CONFIG
});

let timeout = 5000;

class apiCaller {
  static get(route, params, callback) {
    if (params && params.timeout) {
      timeout = params.timeout;
    }

    return instance
      .get(route, {
        params,
        timeout
      })
      .then(res => {
        if ("function" === typeof callback) {
          return callback(res);
        }
        return res;
      })
      .catch(e => {
        if (route.includes("ipify")) {
          if ("function" === typeof callback) {
            return callback();
          }
        } else {
          return e;
        }
      });
  }

  static post(route, body, callback) {
    if (body && body.timeout) {
      timeout = body.timeout;
    }

    return instance
      .post(route, body, { timeout })
      .then(res => {
        if (res.data.success) {
        } else {
        }

        if ("function" === typeof callback) {
          return callback(res);
        }

        return res;
      })
      .catch(e => {
        if ("function" === typeof callback) {
          return callback(e);
        }

        return e;
      });
  }


  static put(route, params, body, callback) {
    if (params && params.timeout) {
      timeout = params.timeout;
    }

    return instance
      .put(route, body, { params, timeout })
      .then(res => {
        if (res.data.success) {
        } else {
        }

        if ("function" === typeof callback) {
          return callback(res);
        }

        return res;
      })
      .catch(e => {
        if ("function" === typeof callback) {
          return callback(e);
        }

        return e;
      });
  }

  static delete(route, params, callback) {
    if (params && params.timeout) {
      timeout = params.timeout;
    }

    return instance
      .delete(route, { params, timeout })
      .then(res => {
        if (res.data.success) {
        } else {
        }

        if ("function" === typeof callback) {
          return callback(res);
        }

        return res;
      })
      .catch(e => {
        if ("function" === typeof callback) {
          return callback(e);
        }

        return e;
      });
  }  
}


export default apiCaller;