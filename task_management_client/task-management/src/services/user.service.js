import apiCaller from "../apiCaller";
import { API_ENPOINTS } from "../contansts/config";

export const GetAllUser = async () => {
    return await apiCaller.get(API_ENPOINTS.users)
};

export const AddUser = async (userObj) => {
    return await apiCaller.post(API_ENPOINTS.users, userObj);
}

export const UpdateUser = async (userObj) => {
    return await apiCaller.put(API_ENPOINTS.users, { id: userObj.id }, userObj);
}

export const DeleteUser = async (userId) => {
    return await apiCaller.delete(API_ENPOINTS.users, {id: userId});
}