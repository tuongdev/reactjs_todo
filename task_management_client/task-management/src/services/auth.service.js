import apiCaller from "../apiCaller";
import { API_ENPOINTS } from "../contansts/config";

export const SignIn = async (name, password) => {
    return await apiCaller.get(API_ENPOINTS.auth + "/signin", { name: name, password: password });
};

export const SignUp = async (userObj) => {
    return await apiCaller.post(API_ENPOINTS.auth + "/signup", userObj);
}
