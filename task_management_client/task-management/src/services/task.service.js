import apiCaller from "../apiCaller";
import { API_ENPOINTS } from "../contansts/config";

export const GetAll = async () => {
    return await apiCaller.get(API_ENPOINTS.tasks);
};

export const AddTask = async (taskObj) => {
    return await apiCaller.post(API_ENPOINTS.tasks, taskObj);
}

export const UpdateTask = async (taskObj) => {
    return await apiCaller.put(API_ENPOINTS.tasks, {id: taskObj.id}, taskObj);
}

export const DeleteTask = async (taskId) => {
    return await apiCaller.delete(API_ENPOINTS.tasks, {id: taskId});
}

