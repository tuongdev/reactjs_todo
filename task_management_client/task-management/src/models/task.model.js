
import {v4 as uuidv4} from 'uuid';

export default class TaskModel {
    constructor({id, name, description, status, userId}) {
        this.id = id || uuidv4()
        this.name = name
        this.description = description
        this.status = status
        this.userId = userId
    }
}