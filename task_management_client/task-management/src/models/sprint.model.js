import {v4 as uuidv4} from 'uuid';

export default class SprintModel {
    constructor({id, name}) {
        this.id = id || uuidv4()
        this.name = name
    }
}