import {v4 as uuidv4} from 'uuid';

export default class ScheduleModel {
    constructor({id, name, creator, room, startTime, endTime }) {
        this.id = id || uuidv4()
        this.name = name
        this.creator = creator
        this.room = room
        this.startTime = startTime
        this.endTime = endTime
    }
}