
import {v4 as uuidv4} from 'uuid';

export default class UserModel {
    constructor({id, name, password, email, roles}) {
        this.id = id || uuidv4()
        this.name = name
        this.password = password
        this.email = email
        this.roles = roles
    }
}