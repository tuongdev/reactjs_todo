const API_CONFIG = 'http://localhost:3003/api';

const API_ENPOINTS = {
    auth: "auth",
    users: "users",
    tasks: "tasks"
}
export { API_CONFIG, API_ENPOINTS };